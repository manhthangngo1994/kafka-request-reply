package com.gauravg.utils;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.concurrent.CompletableFuture;

@Component
public class ReplyingKafkaUtils {

//    private CompletableFuture<?> adapt(RequestReplyFuture<?, ?, ?> requestReplyFuture) {
//        CompletableFuture<?> completableResult = new CompletableFuture<>() {
//            @Override
//            public boolean cancel(boolean mayInterruptIfRunning) {
//                boolean result = requestReplyFuture.cancel(mayInterruptIfRunning);
//                super.cancel(mayInterruptIfRunning);
//                return result;
//            }
//        };
//        // Add callback to the request sending result
//        requestReplyFuture.getSendFuture().addCallback(new ListenableFutureCallback<SendResult<K, V>>() {
//            @Override
//            public void onSuccess(SendResult<K, V> sendResult) {
//                // NOOP
//            }
//            @Override
//            public void onFailure(Throwable t) {
//                completableResult.completeExceptionally(t);
//            }
//        });
//        // Add callback to the reply
//        requestReplyFuture.addCallback(new ListenableFutureCallback<ConsumerRecord<K, R>>() {
//            @Override
//            public void onSuccess(ConsumerRecord<K, R> result) {
//                completableResult.complete(result.value());
//            }
//            @Override
//            public void onFailure(Throwable t) {
//                completableResult.completeExceptionally(t);
//            }
//        });
//        return completableResult;
//    }

}
