package com.gauravg.controller;

import com.gauravg.model.Model;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@RestController
public class SumController {
	
	@Autowired
	ReplyingKafkaTemplate<String, Model,Model> kafkaTemplate;
	
	@Value("${kafka.topic.request-topic}")
	String requestTopic;
	
	@Value("${kafka.topic.requestreply-topic}")
	String requestReplyTopic;
	
	@ResponseBody
	@PostMapping(value="/sum",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Model sum(@RequestBody Model request) throws InterruptedException, ExecutionException, TimeoutException {
		// create producer record
		ProducerRecord<String, Model> record = new ProducerRecord<String, Model>(requestTopic, request);
		// set reply topic in header
		record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, requestReplyTopic.getBytes()));
		record.headers().add(new RecordHeader(KafkaHeaders.PARTITION_ID, "7".getBytes()));
//		record.headers().add(new RecordHeader(KafkaHeaders.REPLY_PARTITION, "6".getBytes()));
		// post in kafka topic
		RequestReplyFuture<String, Model, Model> sendAndReceive = kafkaTemplate.sendAndReceive(record);

		sendAndReceive.getSendFuture().addCallback(new ListenableFutureCallback<SendResult<String, Model>>() {
			@Override
			public void onFailure(Throwable throwable) {
				System.out.println(throwable);
			}

			@Override
			public void onSuccess(SendResult<String, Model> stringModelSendResult) {
				SendResult<String, Model> sendResult = stringModelSendResult;
				sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));

			}
		});

		sendAndReceive.addCallback(new ListenableFutureCallback<ConsumerRecord<String, Model>>() {
			@Override
			public void onFailure(Throwable throwable) {
				System.out.println(throwable);
			}

			@Override
			public void onSuccess(ConsumerRecord<String, Model> modelConsumerRecord) {
				System.out.println(modelConsumerRecord.value());
			}
		});

		// confirm if producer produced successfully
//		SendResult<String, Model> sendResult = sendAndReceive.getSendFuture().get();
//
//		//print all headers
//		sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));
//
//		// get consumer record
		ConsumerRecord<String, Model> consumerRecord = sendAndReceive.get(10, TimeUnit.SECONDS);
		// return consumer value
		return consumerRecord.value();
	}

}
