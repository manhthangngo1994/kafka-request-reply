package com.gauravg.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.gauravg.model.Model;
import org.springframework.transaction.annotation.Transactional;


@Component
public class ReplyingKafkaConsumer {
	 
	 @KafkaListener(topics = "biz")
	 @SendTo
	 @Transactional
//	  public Model listen(Model request) {
	  public Model listen(ConsumerRecord<String, Model> consumerRecord, @Header(KafkaHeaders.CORRELATION_ID) byte[] correlation) {
		 consumerRecord.headers().add(KafkaHeaders.CORRELATION_ID, correlation);
		 Model request = consumerRecord.value();
		 int sum = request.getFirstNumber() + request.getSecondNumber();
		 request.setAdditionalProperty("sum", sum);
		 System.out.println("Gia tri tinh tong: " + sum);
		 return request;
	  }

}
